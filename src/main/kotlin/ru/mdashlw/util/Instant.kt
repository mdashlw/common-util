@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.util

import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneId

/**
 * Obtains an [OffsetDateTime] instance from [Instant]
 *
 * @param zone A time-zone ID, default is system default
 * @return [OffsetDateTime] An instance
 */
inline fun Instant.toOffsetDateTime(zone: ZoneId = ZoneId.systemDefault()): OffsetDateTime =
    OffsetDateTime.ofInstant(this, zone)
