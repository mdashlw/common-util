@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.util

inline fun Boolean.toInt() = if (this) 1 else 0

inline fun Boolean.toString(`true`: String, `false`: String): String =
    if (this) `true` else `false`
