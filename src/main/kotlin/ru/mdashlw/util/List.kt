package ru.mdashlw.util

/**
 * Checks if [element] is contained (ignoring case) by the list
 *
 * @param element An element
 * @return Does the list contain [element]
 */
fun List<String>.containsIgnoreCase(element: String): Boolean = any { it.equals(element, true) }
