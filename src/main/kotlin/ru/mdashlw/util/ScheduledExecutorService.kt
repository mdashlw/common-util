@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.util

import java.time.Duration
import java.util.concurrent.Callable
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

inline fun ScheduledExecutorService.schedule(command: Runnable, duration: Duration): ScheduledFuture<*> =
    schedule(command, duration.seconds, TimeUnit.SECONDS)

inline fun <V> ScheduledExecutorService.schedule(callable: Callable<V>, duration: Duration): ScheduledFuture<V> =
    schedule(callable, duration.seconds, TimeUnit.SECONDS)

inline fun ScheduledExecutorService.scheduleAtFixedRate(
    command: Runnable,
    period: Long,
    unit: TimeUnit
): ScheduledFuture<*> = scheduleAtFixedRate(command, period, period, unit)

inline fun ScheduledExecutorService.scheduleAtFixedRate(command: Runnable, duration: Duration): ScheduledFuture<*> =
    scheduleAtFixedRate(command, duration.seconds, TimeUnit.SECONDS)

inline fun ScheduledExecutorService.scheduleWithFixedDelay(
    command: Runnable,
    delay: Long,
    unit: TimeUnit
): ScheduledFuture<*> = scheduleWithFixedDelay(command, delay, delay, unit)

inline fun ScheduledExecutorService.scheduleWithFixedDelay(command: Runnable, duration: Duration): ScheduledFuture<*> =
    scheduleWithFixedDelay(command, duration.seconds, TimeUnit.SECONDS)
