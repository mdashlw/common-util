@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.util

inline infix fun Double.format(digits: Int) = String.format("%.${digits}f", this)
