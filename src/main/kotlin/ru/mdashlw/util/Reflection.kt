package ru.mdashlw.util

@Suppress("UNCHECKED_CAST")
@Throws(NoSuchFieldException::class, TypeCastException::class)
fun <T> Any.getField(name: String): T =
    javaClass.getDeclaredField(name)
        .apply { isAccessible = true }
        .get(this) as T
