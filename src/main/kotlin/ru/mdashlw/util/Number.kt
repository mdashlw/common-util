@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.util

import java.text.NumberFormat
import java.util.*

@JvmField
val NUMBER_PATTERN = "\\d+".toRegex()

inline fun Number.format(locale: Locale = Locale.US): String = NumberFormat.getInstance(locale).format(this)
