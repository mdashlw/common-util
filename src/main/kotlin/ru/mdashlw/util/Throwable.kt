package ru.mdashlw.util

import java.io.PrintWriter
import java.io.StringWriter

val Throwable.text: String
    get() = StringWriter().apply { printStackTrace(PrintWriter(this)) }.toString()
