package ru.mdashlw.util.thread

import java.util.concurrent.ThreadFactory

class CustomizableThreadFactory(
    private val name: String,
    private val isDaemon: Boolean = false,
    private val priority: Int = Thread.NORM_PRIORITY
) : ThreadFactory {
    private val group: ThreadGroup = System.getSecurityManager()?.threadGroup ?: Thread.currentThread().threadGroup
    private var threadNumber: Int = 1

    override fun newThread(r: Runnable): Thread =
        Thread(group, r, name.format(threadNumber++), 0).apply {
            this.isDaemon = isDaemon
            this.priority = priority
        }
}
