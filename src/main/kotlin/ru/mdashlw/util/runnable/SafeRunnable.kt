package ru.mdashlw.util.runnable

interface SafeRunnable : Runnable {
    fun execute()

    fun handleException(exception: Throwable) {
        exception.printStackTrace()
    }

    override fun run() {
        try {
            execute()
        } catch (exception: Throwable) {
            handleException(exception)
        }
    }
}
