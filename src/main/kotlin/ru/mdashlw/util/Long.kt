@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.util

import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneId

/**
 * Obtains an [Instant] instance from millis
 *
 * @return [Instant] An instance
 */
inline fun Long.toInstant(): Instant = Instant.ofEpochMilli(this)

inline fun Long.toOffsetDateTime(zone: ZoneId = ZoneId.systemDefault()): OffsetDateTime =
    toInstant().toOffsetDateTime(zone)

inline infix fun Long.ratio(other: Long): Double =
    when {
        this == 0L -> 0.0
        other == 0L -> toDouble()
        else -> toDouble() / other.toDouble()
    }
