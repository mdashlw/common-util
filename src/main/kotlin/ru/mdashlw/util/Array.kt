package ru.mdashlw.util

/**
 * Checks if [element] is contained (ignoring case) by the array
 *
 * @param element An element
 * @return Does the array contain [element]
 */
fun Array<String>.containsIgnoreCase(element: String): Boolean = any { it.equals(element, true) }
