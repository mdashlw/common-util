@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.util

inline infix fun Int.average(other: Int) = plus(other) / 2

inline fun Int.average(vararg others: Int) = plus(others.sum()) / (others.size + 1)

inline infix fun Int.ratio(other: Int): Double =
    when {
        this == 0 -> 0.0
        other == 0 -> toDouble()
        else -> toDouble() / other.toDouble()
    }
