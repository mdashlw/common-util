@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.util

import java.util.*

@JvmField
val EXTRA_SPACES_PATTERN = "\\s{2,}".toRegex()

/**
 * Checks if the string is a number
 *
 * @return Is the string a number
 */
inline fun String.isNumber(): Boolean = NUMBER_PATTERN.matches(this)

/**
 * Checks if the string can be casted to [Int]
 *
 * @return Is the string an [Int]
 */
inline fun String.isInt(): Boolean = toIntOrNull() != null

/**
 * Checks if the string can be casted to [Long]
 *
 * @return Is the string a [Long]
 */
inline fun String.isLong(): Boolean = toLongOrNull() != null

/**
 * Checks if the string can be casted to [Boolean]
 *
 * @return Is the string a `true` or `false` (ignoring case)
 */
inline fun String.isBoolean(): Boolean =
    equals("true", true) || equals("false", true)

fun String.isUuid(): Boolean =
    try {
        toUuid()
        true
    } catch (exception: IllegalArgumentException) {
        false
    }

inline fun String.toUuid(): UUID = UUID.fromString(this)

inline fun String.toUuidOrNull(): UUID? =
    try {
        toUuid()
    } catch (exception: IllegalArgumentException) {
        null
    }

/**
 * Removes all occurrences of the [value] in the string
 * @param value A vlaue
 * @param ignoreCase Ignore case?
 * @return A new string with all occurrences of the [value] removed
 */
inline fun String.remove(value: String, ignoreCase: Boolean = false): String = replace(value, "", ignoreCase)

/**
 * Removes decimal number format symbol
 * @return A new string without `,`
 */
inline fun String.removeNumberFormat(): String = remove(",")

inline fun String.removeExtraSpaces(): String = replace(EXTRA_SPACES_PATTERN, " ")

inline fun String.placeholder(name: String, value: Any): String = replace("%$name%", value.toString())

inline infix fun String.placeholder(pair: Pair<String, Any>): String = placeholder(pair.first, pair.second)

fun String.placeholders(vararg pairs: Pair<String, Any>): String {
    var string = this

    pairs.forEach { string = string placeholder it }

    return string
}
