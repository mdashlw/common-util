@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.util

inline operator fun MatchResult.get(index: Int): String = groupValues[index]
