@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.util

import java.util.*

@JvmField
val EMPTY_UUID: UUID = UUID.fromString("00000000-0000-0000-0000-000000000000")

val UUID.isEmpty: Boolean
    get() = this == EMPTY_UUID
