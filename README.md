[ ![Download](https://api.bintray.com/packages/mdashlw/maven/common-util/images/download.svg) ](https://bintray.com/mdashlw/maven/common-util/_latestVersion)
[![pipeline status](https://gitlab.com/mdashlw/common-util/badges/master/pipeline.svg)](https://gitlab.com/mdashlw/common-util/commits/master)

# Common Util

Common utils and extensions for Kotlin

## Importing

Replace `VERSION` with the latest version above.

### Gradle Groovy DSL

<details><summary>build.gradle</summary>
<p>

```gradle
repositories {
    jcenter()
}

dependencies {
    implementation 'ru.mdashlw.util:common-util:VERSION'
}
```

</p>
</details>

### Gradle Kotlin DSL

<details><summary>build.gradle.kts</summary>
<p>

```kotlin
repositories {
    jcenter()
}

dependencies {
    implementation("ru.mdashlw.util:common-util:VERSION")
}
```

</p>
</details>

### Maven

<details><summary>pom.xml</summary>
<p>

```xml
<depedencies>
    <dependency>
        <groupId>ru.mdashlw.util</groupId>
        <artifactId>common-util</artifactId>
        <version>VERSION</version>
  </dependency>
</depedencies>

<repositories>
    <repository>
      <id>jcenter</id>
      <name>JCenter</name>
      <url>https://jcenter.bintray.com/</url>
    </repository>
</repositories>
```

</p>
</details>

## License

The project is licensed under the **[MIT license](https://choosealicense.com/licenses/mit/)**.